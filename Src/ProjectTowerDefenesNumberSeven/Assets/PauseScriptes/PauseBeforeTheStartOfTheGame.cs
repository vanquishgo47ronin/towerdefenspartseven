using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using System;

public class PauseBeforeTheStartOfTheGame : MonoBehaviour
{
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("StopFromStart");
    }

    IEnumerable StopFromStart()
    {
        yield return new WaitForSeconds(10f);
        StopCoroutine("StopFromStart");
    }
}
