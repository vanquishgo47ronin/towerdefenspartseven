using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "02", fileName = "IndeacatePeh")]
public class EnomyVersioTwoZero : ScriptableObject
{
    public List<EnomOne> _enomOne;
}

[System.Serializable]
public class EnomOne
{
    [SerializeField] private int _numberEnom;
    [SerializeField] private GameObject _objEnom;

    public int Number => _numberEnom;

    public GameObject Obj => _objEnom;
}