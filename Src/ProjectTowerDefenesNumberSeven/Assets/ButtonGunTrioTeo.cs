using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class ButtonGunTrioTeo : MonoBehaviour
{
    [SerializeField] private GameObject _sharFromGunTrio;

    //[SerializeField] private TimerFromJobGun _timerFromJobGun;

    [SerializeField] private TuretTrioTeoGun _turetTrioTeoGun;

    [SerializeField] private ScriptFromGunTrioTeo _scriptFromGunTrioTeo;

    [SerializeField] private Button _butonTrio;

    //private float _x = 19.5f;

    // Start is called before the first frame update
    void Start()
    {
        //_sharFromGunTrio.transform.Rotate(new Vector3(_x, 0, 0));
    }

    public void OnClikeFromOnGunTrio()
    {
        //_timerFromJobGun.ActivateadCorutinu();
        _turetTrioTeoGun.enabled = true;
        _scriptFromGunTrioTeo.enabled = true;
        
        _butonTrio.interactable = false;
        _scriptFromGunTrioTeo.IreadyFire();
    }

    public void OffGun()
    {
        //_stvolGunTrio.transform.rotation.z = _z;
    }

    public void ButtonOff()
    {
        
    }

    public void ButonOn()
    {
        _butonTrio.interactable = true;
        _scriptFromGunTrioTeo.enabled = false;
        _turetTrioTeoGun.enabled = false;
    }
}
