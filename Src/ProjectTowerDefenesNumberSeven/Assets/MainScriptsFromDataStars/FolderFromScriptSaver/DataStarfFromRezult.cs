using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System.IO;
using System;
using UnityEngine;
using Newtonsoft.Json;

public class DataStarfFromRezult : MonoBehaviour
{
    // ����������� ������.
    private MainStarsDara _mainStarsData;

    public const string Main_Stars_Dara_Key = "Data_Key"; // ����

    // Start is called before the first frame update
    void Start()
    {
        //JsonConvert.DeserializeObject();
        //JsonConvert.SerializeObject; // �������� �� ������� ������

        Debug.Log(Resources.Load<TextAsset>("starsdata").text);

        // ���� � ���� ���������, � ����� �� ����������.
        if (string.IsNullOrEmpty(PlayerPrefs.GetString(Main_Stars_Dara_Key, "")))
        {
            // ����� Get �������� �� � ������.

            Debug.Log("Prefs are empty. Saving default value");

            //����� ������
            PlayerPrefs.SetString(Main_Stars_Dara_Key, Resources.Load<TextAsset>("starsdata").text); // �� ������ ������ ��� ������ � ����� ���� ��.
        }
        // ����� �����

        //Debug.Log(Main_Stars_Dara_Key);

        this._mainStarsData = JsonConvert.DeserializeObject<MainStarsDara>(PlayerPrefs.GetString(Main_Stars_Dara_Key));

        //Debug.Log(JsonConvert.DeserializeObject<MainStarsDara>(PlayerPrefs.GetString(Main_Stars_Dara_Key)));
        Debug.Log(this._mainStarsData);
        
        Debug.Log($"SceneUpgrateTree our coins = {this._mainStarsData._countStarsWithFiveLevel}");
    }

    private void OnDisable()
    {
        Debug.Log("oNdISABLE: Save data...");
        PlayerPrefs.SetString(Main_Stars_Dara_Key, JsonConvert.SerializeObject(this._mainStarsData)); // �� ��������� ��������� � ������.
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            this._mainStarsData._countStarsWithFiveLevel++;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            SceneManager.LoadScene("LevelSceneOne");
        }
    }
}
