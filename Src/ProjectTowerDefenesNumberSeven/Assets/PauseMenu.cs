using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] private Canvas _playUI;
    [SerializeField] private Canvas _pauseUI;

    public void ExitToMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(0);
    }

    public void ContinueGame()
    {
        _pauseUI.gameObject.SetActive(false);
        _playUI.gameObject.SetActive(true);
        Time.timeScale = 1f;
    }

    private void Update()
    {
        Time.timeScale = 0f;
    }

    public void Restart()
    {
        //SceneManager.LoadScene(0);
    }
}
