using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseScript : MonoBehaviour
{
    [SerializeField] private Canvas _playUI;
    [SerializeField] private Canvas _pauseUI;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            _playUI.gameObject.SetActive(false);
            Time.timeScale = 0f;
            _pauseUI.gameObject.SetActive(true);
        }
    }
}
