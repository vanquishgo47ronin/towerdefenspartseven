using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NamTowers;
using TMPro;

public class TowerBrain : MonoBehaviour
{
    

    //[SerializeField] private Canvas _playCanvas;

    [SerializeField] private Canvas _towerCanvas;

    [SerializeField] private TextMeshProUGUI _text;

    [SerializeField] private GameObject _thisTower;

    [SerializeField] private NameTowers _nameTowers;
    [SerializeField] private LevelTowers _level;

    //[SerializeField] private UpgrateTower _upgrateTower;
    //[SerializeField] private SaelTower _saleTower;

    //[SerializeField] private HelpUpgrateManeger _helpUpgrateManeger;

    //private HelpSaleManeger _helpSaleManeger;

    [SerializeField] private SaleUpgrateManeger _saleUpgrateManeger;

    //private HelpBildMenager _helpBildMenager;

    [SerializeField] private CrystalBank _crystalBank;

    private bool _onClikeSaleAndUpgrate = false;

    private void Awake()
    {
        //_helpUpgrateManeger = new HelpUpgrateManeger();
        //_helpSaleManeger = new HelpSaleManeger();

        //_helpBildMenager = new HelpBildMenager();

        //_helpUpgrateManeger.MetodHelpUpgrateManeger(_levelTowers, _nameTowers/*, _text*/);
    }

    private void Update()
    {
        
    }

    private void OnMouseDown()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _onClikeSaleAndUpgrate = true;
            //_playCanvas.gameObject.SetActive(false);
            _towerCanvas.gameObject.SetActive(true);
            _saleUpgrateManeger.ReceiveDataOnTheTower(_nameTowers, _level, _thisTower);
            //_saleTower.ReceiveDataOnTheTower(_nameTowers, _level, _thisTower);
        }
        _onClikeSaleAndUpgrate = false;
    }

    public void GoodBuyTowerSaleYou()
    {
        //ResetTheAnswer();

        //DestroyObject(this._thisTower);
        Destroy(this._thisTower);
    }

    public void ResetTheAnswer()
    {
        //_canChoose = false;
    }

    public void Resurrection()
    {
        //_playCanvas.gameObject.SetActive(true);
    }

    /*
    public void ClouseChoise()
    {
        _towerCanvas.gameObject.SetActive(false);
    }*/
}
