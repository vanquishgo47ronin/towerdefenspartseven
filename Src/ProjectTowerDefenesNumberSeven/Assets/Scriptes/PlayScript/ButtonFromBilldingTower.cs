using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using NamTowers;

public class ButtonFromBilldingTower : MonoBehaviour
{
    [SerializeField] private ActiveOffButtunManegare _activeOffButtunManegare;

    /*[SerializeField] private DataMachineTower _towerMachineLevelOne;
    [SerializeField] private DataAntiTank _towerAntiTankLevelOne;
    [SerializeField] private DataPlazmaganTower _towerPlazmaganLevelOne;
    [SerializeField] private DataAntiTank _towerAntiFlyLevelOne;*/
    //[SerializeField] private DataTransfer _dataTransfer;
    //private BuildingTower _buildingTower;
    //_buildingTower = new BuildingTower(_platformForTower);
    //private TowerSelection _towerSelection;
    //[SerializeField] private Button _buttonFromBilld;
    [SerializeField] private DataMachineTower _dataMachineTower;
    [SerializeField] private DataAntiTank _dataAntiTank;
    [SerializeField] private DataPlazmaganTower _dataPlazmaganTower;
    [SerializeField] private DataAntiFlyTower _dataAntiAirStrikeFlyTower;

    [SerializeField] private CrystalBank _crystalBank;

    [SerializeField] private Button _buttonMachine;
    [SerializeField] private Button _buttonAntiTank;
    [SerializeField] private Button _buttonPlazmagan;
    [SerializeField] private Button _buttonAntiFly;

    private BuildingTower _buildingTower;

    private PriceComparisonOfTowersWithBank _priceComparisonOfTowersWithBank;

    TowerSelection _towerSelection;
    [SerializeField] private Button _button;
    [SerializeField] private NameTowers _name;

    private bool _diseActive = false;

    //[SerializeField] private PlatformForTower _platformForTower; 

    [SerializeField] private HelpBildMenager _helpBildMenager;

    private void Awake()
    {
        //_priceComparisonOfTowersWithBank = new PriceComparisonOfTowersWithBank();
        _buildingTower = new BuildingTower(_dataMachineTower, _dataAntiTank, _dataPlazmaganTower, _dataAntiAirStrikeFlyTower/*, _towerMachineLevelOne, _towerAntiTankLevelOne, _towerPlazmaganLevelOne, _towerAntiFlyLevelOne*/);
        //_dataTransfer = new DataTransfer(_buildingTower);
        _towerSelection = new TowerSelection();
        //_helpBildMenager = new HelpBildMenager();
    }

    public void BildingTowerMachine()
    {
        if (/*actualPosition == true && */_crystalBank.Resources >= 100/*_textMachine.color == Color.white*/)
        {
            _buttonMachine.interactable = true;
            for (int i = 0; i < _dataMachineTower.ModelTowersMachine.Count;)
            {
                if (i == 0)
                {
                    GameObject _firstMachineTower = _dataMachineTower.ModelTowersMachine[0];
                    _helpBildMenager.CollectingTowerData(_firstMachineTower);
                    _helpBildMenager.MakingTheFinalResult();
                    _crystalBank.Resources -= 100;
                    _activeOffButtunManegare.ActivateButtons(_diseActive);
                    //DisableButtons();
                }
                break;
            }
            _towerSelection.ChooseTower(_name, _buildingTower);
        }
        else if (/*actualPosition == false &&*/ _crystalBank.Resources < 100/*_textMachine.color == Color.red*/)
        {
            _buttonMachine.interactable = false;
        }
    }

    public void BildingTowerAntiTank()
    {
        if (/*actualPosition == true &&*/ _crystalBank.Resources >= 210/*_textAntiTank.color == Color.white*/)
        {
            _buttonAntiTank.interactable = true;
            for (int i = 0; i < _dataAntiTank.ModelTowersAntiTank.Count;)
            {
                if (i == 0)
                {
                    GameObject _firstMachineTower = _dataAntiTank.ModelTowersAntiTank[0];
                    _helpBildMenager.CollectingTowerData(_firstMachineTower);
                    _helpBildMenager.MakingTheFinalResult();
                    _crystalBank.Resources -= 210;
                    _activeOffButtunManegare.ActivateButtons(_diseActive);
                    //DisableButtons();
                }
                break;
            }
            _towerSelection.ChooseTower(_name, _buildingTower);
        }
        else if (/*actualPosition == false &&*/ _crystalBank.Resources < 210/*_textAntiTank.color == Color.red*/)
        {
            _buttonAntiTank.interactable = false;
        }
    }

    public void BildingTowerPlazmagan()
    {
        if (/*actualPosition == true &&*/ _crystalBank.Resources >= 350/*_textPlazmagan.color == Color.white*/)
        {
            _buttonPlazmagan.interactable = true;
            for (int i = 0; i < _dataPlazmaganTower.ModelTowersPlazmagan.Count;)
            {
                if (i == 0)
                {
                    GameObject _firstMachineTower = _dataPlazmaganTower.ModelTowersPlazmagan[0];
                    _helpBildMenager.CollectingTowerData(_firstMachineTower);
                    _helpBildMenager.MakingTheFinalResult();
                    _crystalBank.Resources -= 350;
                    _activeOffButtunManegare.ActivateButtons(_diseActive);
                    //DisableButtons();
                }
                break;
            }
            _towerSelection.ChooseTower(_name, _buildingTower);
        }
        else if (/*actualPosition == false &&*/ _crystalBank.Resources < 350/*_textPlazmagan.color == Color.red*/)
        {
            _buttonPlazmagan.interactable = false;
        }
    }

    public void BildingTowerAntiAirStrikeFly()
    {
        if (/*actualPosition == true &&*/ _crystalBank.Resources >= 150/*_textAntiTank.color == Color.white*/)
        {
            _buttonAntiFly.interactable = true;
            for (int i = 0; i < _dataAntiAirStrikeFlyTower.ModelTowersAntiFly.Count;)
            {
                if (i == 0)
                {
                    GameObject _firstMachineTower = _dataAntiAirStrikeFlyTower.ModelTowersAntiFly[0];
                    _helpBildMenager.CollectingTowerData(_firstMachineTower);
                    _helpBildMenager.MakingTheFinalResult();
                    _crystalBank.Resources -= 150;
                    _activeOffButtunManegare.ActivateButtons(_diseActive);
                    //DisableButtons();
                }
                break;
            }
            _towerSelection.ChooseTower(_name, _buildingTower);
        }
        else if (/*actualPosition == false &&*/ _crystalBank.Resources < 150/*_textAntiTank.color == Color.red*/)
        {
            //_buttonAntiFly.interactable = false;
        }
    }

    public void SendLinkFromBuildTower()
    {
        
        
    }
    
    /*
    public void DisableButtons()
    {
        _buttonMachine.interactable = false;
        _buttonAntiTank.interactable = false;
        _buttonPlazmagan.interactable = false;
        _buttonAntiFly.interactable = false;
    }*/

    
    public void DisableButtonsMachineTower()
    {
        _buttonMachine.interactable = false;
    }

    
    public void DisableButtonsAntiTank()
    {
        _buttonAntiTank.interactable = false;
    }

    
    public void DisableButtonsPlazmaganTower()
    {
        _buttonPlazmagan.interactable = false;
    }

    
    public void DisableButtonsAntiFlyTower()
    {
        _buttonAntiFly.interactable = false;
    }
    

    /*private void ChooseTower(NameTowers namTow)
    {
        if (namTow == NameTowers.Machine_Tower)
        {
            Debug.Log("� ������!");
        }
        else if (namTow == NameTowers.AntiTank)
        {
            Debug.Log("� ��������������� ������!");
        }
        else if (namTow == NameTowers.Plazmagan)
        {
            Debug.Log("� ���������!");
        }
        else if (namTow == NameTowers.AntiFly)
        {
            Debug.Log("� ���!");
        }
    }*/
}
