using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegularTime : MonoBehaviour
{
    [SerializeField] private GameObject _objAccelerated;

    [SerializeField] private GameObject _objRegular;

    [SerializeField] private GameObject _objButtonAccelerated;

    [SerializeField] private GameObject _objButtonRegular;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Regular()
    {
        _objRegular.SetActive(true);
        _objAccelerated.SetActive(false);
        _objButtonAccelerated.SetActive(true);
        _objButtonRegular.SetActive(false);
        
    }
}
