using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcceleratedTime : MonoBehaviour
{
    [SerializeField] private GameObject _objAccelerated;

    [SerializeField] private GameObject _objRegular;

    [SerializeField] private GameObject _objButtonAccelerated;

    [SerializeField] private GameObject _objButtonRegular;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Accelerated()
    {
        _objAccelerated.SetActive(true);
        _objRegular.SetActive(false);
        _objButtonRegular.SetActive(true);
        _objButtonAccelerated.SetActive(false);
    }
}
