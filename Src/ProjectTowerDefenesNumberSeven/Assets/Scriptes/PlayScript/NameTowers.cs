namespace NamTowers
{
    public enum NameTowers
    {
        MachineTower,
        Plazmagan,
        AntiFly,
        AntiTank
    };

    public enum LevelTowers
    {
        LEVEL1,
        LEVEL2,
        LEVEL3,
    };
}