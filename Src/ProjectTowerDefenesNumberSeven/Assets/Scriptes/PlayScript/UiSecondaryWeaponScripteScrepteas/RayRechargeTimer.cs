using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class RayRechargeTimer : MonoBehaviour
{
    private Button _buttonRay;

    private void Start()
    {
        _buttonRay = GetComponent<Button>();
        StartCoroutine("RechargeRay");
    }

    public void EnableCoroutine()
    {
        StartCoroutine("RechargeRay");
    }

    IEnumerator RechargeRay()
    {
        while (true)
        {
            _buttonRay.image.fillAmount += 0.1f;
            if (_buttonRay.image.fillAmount == 1)
            {
                StopCoroutine("RechargeRay");
            }
            yield return new WaitForSeconds(2f);
        }
    }
}
