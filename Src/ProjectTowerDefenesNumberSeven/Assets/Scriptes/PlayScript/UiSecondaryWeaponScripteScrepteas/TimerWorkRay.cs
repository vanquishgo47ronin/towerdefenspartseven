using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerWorkRay : MonoBehaviour
{
    private float _maxTime = 700f;

    public float _minTimer = 0f;

    [SerializeField] private GameObject _myRay;
    //[SerializeField] private ParticleSystem _myRay;

    [SerializeField] private TimerFromOnRay _timerFromOnRay;

    // Start is called before the first frame update
    void Start()
    {
        //_timerFromOnRay.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        _minTimer += 1;
        if (_minTimer == _maxTime)
        {
            _minTimer = 0;
            _timerFromOnRay.enabled = true;
            this.enabled = false;
            _myRay.SetActive(false);
            //_myRay.gameObject.SetActive(false);
        }
    }
}
