using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

public class ButtonRay : MonoBehaviour
{
    //[SerializeField] private Image _image;
    //[SerializeField] private Transform _aimRay;
    [SerializeField] private GameObject _point;

    [SerializeField] private Camera _mainCameraOne;
    [SerializeField] private Camera _mainCameraTwo;

    [SerializeField] private Canvas _canvasPlay;
    [SerializeField] private Canvas _canvasTactical;

    [SerializeField] private TimerWorkRay _timerFromRay;

    public void OnClikeRay()
    {
        _canvasPlay.gameObject.SetActive(false);
        _mainCameraOne.gameObject.SetActive(false);
        _mainCameraTwo.gameObject.SetActive(true);
        _canvasTactical.gameObject.SetActive(true);
        _point.SetActive(true);
    }

    public void ClouseInterfase()
    {
        _canvasPlay.gameObject.SetActive(true);
        _mainCameraOne.gameObject.SetActive(true);
        _mainCameraTwo.gameObject.SetActive(false);
        _canvasTactical.gameObject.SetActive(false);
    }
}
