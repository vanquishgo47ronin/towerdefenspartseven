using System.Collections;
using System.Collections.Generic;
using UpgradeLevels;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class UpgradeParticle : MonoBehaviour
{
    [SerializeField] private ParticleSystem _ionRayTiles;

    private ParticleSystem.ShapeModule _ionRayShape;

    private float _x;

    public bool _myNameLevelTwo = false;
    public bool _myNameLevelThree = false;
    public bool _myNameLevelFore = false;

    // Start is called before the first frame update
    void Start()
    {
        _ionRayTiles = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void NewMetod(UpgradeLevel level, float x)
    {
        _x = x;
    }

    [System.Obsolete]
    public void ImprovementOverTheBeamHappened(UpgradeLevel level)
    {
        if (level == UpgradeLevel.LEVEL2)
        {
            //_x = 3.22f;
            //_ionRay.startLifetime = _x;
            PlayerPrefs.SetFloat("2", _x);
            _myNameLevelTwo = true;
            _x = PlayerPrefs.GetFloat("2");
            //_ionRayShape.radius = 0.25f;
            Debug.Log("� ������� �� 2-�� ������");
            //_ionRay.startLifetime = _x;
        }
        else if (level == UpgradeLevel.LEVEL3)
        {
            _x = 3.3f;
            //_ionRay.startLifetime = _x;
            _myNameLevelTwo = false;
            _myNameLevelThree = true;
            //_ionRayShape.radius = 0.4f;
            Debug.Log("� ������� �� 3-�� ������");
        }
        else if (level == UpgradeLevel.LEVEL4)
        {
            var trails = _ionRayTiles.trails;
            //_ionRay.startLifetime = 3.7f;
            trails.minVertexDistance = 1.14f;
            //_ionRay.startColor = new Color(255f,109f,4f,255f);
            //_ionRayEmission.rateOverTime = 100f;
            //_ionRayShape.radius = 2.7f;
            Debug.Log("� ������� �� 4-�� ������");

        }
    }
}
