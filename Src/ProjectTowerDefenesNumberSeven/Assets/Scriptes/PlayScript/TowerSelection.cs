using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NamTowers;

public class TowerSelection
{
    //NameTowers _nameTower;
    public void ChooseTower(NameTowers nameTower, BuildingTower buildingTower)
    {
        if (nameTower == NameTowers.MachineTower)
        {
            buildingTower.ChooseAtTower(nameTower);
        }
        else if (nameTower == NameTowers.AntiTank)
        {
            buildingTower.ChooseAtTower(nameTower);
        }
        else if (nameTower == NameTowers.Plazmagan)
        {
            buildingTower.ChooseAtTower(nameTower);
        }
        else if (nameTower == NameTowers.AntiFly)
        {
            buildingTower.ChooseAtTower(nameTower);
        }
    }
}
