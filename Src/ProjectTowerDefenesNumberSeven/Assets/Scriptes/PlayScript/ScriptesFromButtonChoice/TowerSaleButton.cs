using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerSaleButton : MonoBehaviour
{
    [SerializeField] private CrystalBank _crystalBank;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Sell()
    {
        _crystalBank.Resources += 0;
        Destroy(this.gameObject);
    }
}
