using System.Collections;
using System.Collections.Generic;
using UnityEngine.UIElements;
using NamTowers;
using UnityEngine;
using TMPro;

public class SaleUpgrateManeger : MonoBehaviour
{
    //private TextMeshProUGUI _textSale;
    //private TextMeshProUGUI _textUpgrate;

    [SerializeField] private DataMachineTower _dataMachineTower;
    [SerializeField] private DataAntiTank _dataAntiTank;
    [SerializeField] private DataPlazmaganTower _dataPlazmaganTower;
    [SerializeField] private DataAntiFlyTower _dataAntiAirStrikeFlyTower;

    [SerializeField] private CrystalBank _crystalBank;

    private NameTowers _nameTowers;
    private LevelTowers _level;

    private GameObject _objectTower;

    [SerializeField] private HelpBildMenager _helpBildMenager;

    /*public void ReceiveDataOnTheCanvasTower(TextMeshProUGUI textSale, TextMeshProUGUI textUpgrate)
    {
        _textSale = textSale;
        _textUpgrate = textUpgrate;
    }*/

    public void ReceiveDataOnTheTower(NameTowers nameTowers, LevelTowers level, GameObject tower)
    {
        _nameTowers = nameTowers;
        _level = level;
        _objectTower = tower;
    }

    public void SaleTower()
    {
        if (_level == LevelTowers.LEVEL1 && _nameTowers == NameTowers.MachineTower)
        {
            //_textSale.text = $"{100}";
            _crystalBank.Resources += 100;
            //GameObject _objTwoMachineTower = _dataMachineTower.ModelTowersMachine[1];
            //_helpBildMenager.ReplaceTower(_objTwoMachineTower);

            Destroy(_objectTower);
        }
        else if (_level == LevelTowers.LEVEL2 && _nameTowers == NameTowers.MachineTower)
        {
            _crystalBank.Resources += 240;
            //GameObject _objThreeMachineTower = _dataMachineTower.ModelTowersMachine[2];
            //_helpBildMenager.ReplaceTower(_objThreeMachineTower);

            Destroy(_objectTower);
        }
        else if (_level == LevelTowers.LEVEL3 && _nameTowers == NameTowers.MachineTower)
        {
            _crystalBank.Resources += 420;
            //GameObject _objThreeMachineTower = _dataMachineTower.ModelTowersMachine[2];
            //_helpBildMenager.ReplaceTower(_objThreeMachineTower);

            Destroy(_objectTower);
        }
        else if (_level == LevelTowers.LEVEL1 && _nameTowers == NameTowers.AntiTank)
        {
            _crystalBank.Resources += 210;
            //GameObject _objTwoAntiTank = _dataAntiTank.ModelTowersAntiTank[1];
            //_helpBildMenager.ReplaceTower(_objTwoAntiTank);

            Destroy(_objectTower);
        }
        else if (_level == LevelTowers.LEVEL2 && _nameTowers == NameTowers.AntiTank)
        {
            _crystalBank.Resources += 510;
            //GameObject _objThreeAntiTank = _dataAntiTank.ModelTowersAntiTank[2];
            //_helpBildMenager.ReplaceTower(_objThreeAntiTank);

            Destroy(_objectTower);
        }
        else if (_level == LevelTowers.LEVEL3 && _nameTowers == NameTowers.AntiTank)
        {
            _crystalBank.Resources += 860;
            //GameObject _objThreeMachineTower = _dataMachineTower.ModelTowersMachine[2];
            //_helpBildMenager.ReplaceTower(_objThreeMachineTower);

            Destroy(_objectTower);
        }
        else if (_level == LevelTowers.LEVEL1 && _nameTowers == NameTowers.Plazmagan)
        {
            _crystalBank.Resources += 350;
            //GameObject _objTwoPlazmagan = _dataPlazmaganTower.ModelTowersPlazmagan[1];
            //_helpBildMenager.ReplaceTower(_objTwoPlazmagan);

            Destroy(_objectTower);
        }
        else if (_level == LevelTowers.LEVEL2 && _nameTowers == NameTowers.Plazmagan)
        {
            _crystalBank.Resources += 750;
            //GameObject _objThreePlazmagan = _dataPlazmaganTower.ModelTowersPlazmagan[2];
            //_helpBildMenager.ReplaceTower(_objThreePlazmagan);

            Destroy(_objectTower);
        }
        else if (_level == LevelTowers.LEVEL3 && _nameTowers == NameTowers.Plazmagan)
        {
            _crystalBank.Resources += 1200;
            //GameObject _objThreeMachineTower = _dataMachineTower.ModelTowersMachine[2];
            //_helpBildMenager.ReplaceTower(_objThreeMachineTower);

            Destroy(_objectTower);
        }
        else if (_level == LevelTowers.LEVEL1 && _nameTowers == NameTowers.AntiFly)
        {
            _crystalBank.Resources += 150;
            //GameObject _objTwoAntiFly = _dataAntiAirStrikeFlyTower.ModelTowersAntiFly[1];
            //_helpBildMenager.ReplaceTower(_objTwoAntiFly);

            Destroy(_objectTower);
        }
        else if (_level == LevelTowers.LEVEL2 && _nameTowers == NameTowers.AntiFly)
        {
            _crystalBank.Resources += 340;
            //GameObject _objThreeAntiFly = _dataAntiAirStrikeFlyTower.ModelTowersAntiFly[2];
            //_helpBildMenager.ReplaceTower(_objThreeAntiFly);

            Destroy(_objectTower);
        }
        else if (_level == LevelTowers.LEVEL3 && _nameTowers == NameTowers.AntiFly)
        {
            _crystalBank.Resources += 570;
            //GameObject _objThreeMachineTower = _dataMachineTower.ModelTowersMachine[2];
            //_helpBildMenager.ReplaceTower(_objThreeMachineTower);

            Destroy(_objectTower);
        }
    }

    public void Upgrate()
    {
        if (_level == LevelTowers.LEVEL1 && _nameTowers == NameTowers.MachineTower && _crystalBank.Resources >= 140)
        {
            //_textUpgrate.text = $"{140}";

            _crystalBank.Resources -= 140;
            GameObject _objTwoMachineTower = _dataMachineTower.ModelTowersMachine[1];
            //Debug.Log(_oldTower);
            //_helpBildMenager.CollectingTowerData(_oldTower);
            //_helpBildMenager.ReplaceTower(_objTwoMachineTower);

            GameObject _objTower = Instantiate(_objTwoMachineTower, _objectTower.transform.position, _objectTower.transform.rotation);

            _objTower.transform.position = _objectTower.transform.position;
            //_objectTower = _dataMachineTower.ModelTowersMachine[1];
            Destroy(_objectTower);
        }
        else if (_level == LevelTowers.LEVEL2 && _nameTowers == NameTowers.MachineTower && _crystalBank.Resources >= 180)
        {
            _crystalBank.Resources -= 180;
            GameObject _objThreeMachineTower = _dataMachineTower.ModelTowersMachine[2];
            //_helpBildMenager.ReplaceTower(_objThreeMachineTower);

            GameObject _objTower = Instantiate(_objThreeMachineTower, _objectTower.transform.position, _objectTower.transform.rotation);

            _objTower.transform.position = _objectTower.transform.position;
            //_objectTower = _dataMachineTower.ModelTowersMachine[2];

            Destroy(_objectTower);
        }
        else if (_level == LevelTowers.LEVEL1 && _nameTowers == NameTowers.AntiTank && _crystalBank.Resources >= 300)
        {
            _crystalBank.Resources -= 300;
            GameObject _objTwoAntiTank = _dataAntiTank.ModelTowersAntiTank[1];
            //_helpBildMenager.ReplaceTower(_objTwoAntiTank);
            GameObject _objTower = Instantiate(_objTwoAntiTank, _objectTower.transform.position, _objectTower.transform.rotation);
            _objTower.transform.position = _objectTower.transform.position;
            Destroy(_objectTower);
        }
        else if (_level == LevelTowers.LEVEL2 && _nameTowers == NameTowers.AntiTank && _crystalBank.Resources >= 350)
        {
            _crystalBank.Resources -= 350;
            GameObject _objThreeAntiTank = _dataAntiTank.ModelTowersAntiTank[2];
            //_helpBildMenager.ReplaceTower(_objThreeAntiTank);
            GameObject _objTower = Instantiate(_objThreeAntiTank, _objectTower.transform.position, _objectTower.transform.rotation);
            _objTower.transform.position = _objectTower.transform.position;
            Destroy(_objectTower);
        }
        else if (_level == LevelTowers.LEVEL1 && _nameTowers == NameTowers.Plazmagan && _crystalBank.Resources >= 400)
        {
            _crystalBank.Resources -= 400;
            GameObject _objTwoPlazmagan = _dataPlazmaganTower.ModelTowersPlazmagan[1];
            //_helpBildMenager.ReplaceTower(_objTwoPlazmagan);
            GameObject _objTower = Instantiate(_objTwoPlazmagan, _objectTower.transform.position, _objectTower.transform.rotation);
            _objTower.transform.position = _objectTower.transform.position;
            Destroy(_objectTower);
        }
        else if (_level == LevelTowers.LEVEL2 && _nameTowers == NameTowers.Plazmagan && _crystalBank.Resources >= 450)
        {
            _crystalBank.Resources -= 450;
            GameObject _objThreePlazmagan = _dataPlazmaganTower.ModelTowersPlazmagan[2];
            //_helpBildMenager.ReplaceTower(_objThreePlazmagan);
            GameObject _objTower = Instantiate(_objThreePlazmagan, _objectTower.transform.position, _objectTower.transform.rotation);
            _objTower.transform.position = _objectTower.transform.position;
            Destroy(_objectTower);
        }
        else if (_level == LevelTowers.LEVEL1 && _nameTowers == NameTowers.AntiFly && _crystalBank.Resources >= 190)
        {
            _crystalBank.Resources -= 190;
            GameObject _objTwoAntiFly = _dataAntiAirStrikeFlyTower.ModelTowersAntiFly[1];
            //_helpBildMenager.ReplaceTower(_objTwoAntiFly);
            GameObject _objTower = Instantiate(_objTwoAntiFly, _objectTower.transform.position, _objectTower.transform.rotation);
            _objTower.transform.position = _objectTower.transform.position;
            Destroy(_objectTower);
        }
        else if (_level == LevelTowers.LEVEL2 && _nameTowers == NameTowers.AntiFly && _crystalBank.Resources >= 230)
        {
            _crystalBank.Resources -= 230;
            GameObject _objThreeAntiFly = _dataAntiAirStrikeFlyTower.ModelTowersAntiFly[2];
            //_helpBildMenager.ReplaceTower(_objThreeAntiFly);
            GameObject _objTower = Instantiate(_objThreeAntiFly, _objectTower.transform.position, _objectTower.transform.rotation);
            _objTower.transform.position = _objectTower.transform.position;
            Destroy(_objectTower);
        }
    }
}
