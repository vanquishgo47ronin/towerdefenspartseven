using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelpBildMenager : MonoBehaviour
{
    private GameObject _activePlatforme;
    private GameObject _activeTower;

    private bool _resultBildingGood = false;

    private bool _upgraerAllow = false;

    private PlatformForTower _platformForTower;

    //[SerializeField] private TowerBrain _towerBrain;

    private void Awake()
    {
        _platformForTower = GetComponent<PlatformForTower>();//new PlatformForTower();
    }

    public void CollectPlatformData(GameObject _platform)
    {
        _activePlatforme = _platform;
    }

    public void CollectingTowerData(GameObject _tower)
    {
        _activeTower = _tower;
    }

    public void CollectionDataBool( bool _bolTrue)
    {
        _resultBildingGood = _bolTrue;
    }

    public void MakingTheFinalResult()
    {
        if (_resultBildingGood)
        {
            GameObject _obj = Instantiate(_activeTower, _activePlatforme.transform.position, _activePlatforme.transform.rotation);
            _obj.transform.SetParent(_activePlatforme.transform);
            
            _resultBildingGood = false;
        }

        //Instantiate(obj, _platform.transform.position, _platform.transform.rotation);
        //_platform.gameObject.SetActive(false);

        //_activePlatforme.SetActive(false);

        // ����� ������� ������ �����
        // 1. _activePlatforme.SetActive(true);
        // 2. _activePlatforme.transform.SetParent(null);
        // 3. Destroy(_activeTower);
        // 4. Destroy(GetComponent<MeshRenderer>(_activeTower)); // ������� Mesh renderer � ����������.

        //_platformForTower.StartBuilding();
    }

    public void ReplaceTower(GameObject newObjTower)//, GameObject oldTower)
    {

        GameObject _objTower = Instantiate(newObjTower, _activePlatforme.transform.position, _activePlatforme.transform.rotation);
        _objTower.transform.SetParent(_activePlatforme.transform);
        //_towerBrain.ResetTheAnswer();
    }

    public void SaleTower(GameObject newObjTower)//, GameObject oldTower)
    {

        GameObject _objTower = Instantiate(newObjTower, _activePlatforme.transform.position, _activePlatforme.transform.rotation);
        _objTower.transform.SetParent(_activePlatforme.transform);
    }
}
