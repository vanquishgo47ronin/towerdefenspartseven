using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "BunkSafe", fileName = "SafePlanets")]

public class SafeWithPlanets : ScriptableObject
{
    [Range(0, 15)]
    public int CountPlanets;
}
