using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : MonoBehaviour
{
    //[SerializeField] private GameObject _platformm;
    [SerializeField] private Transform _platform;

    private bool _readyForConstruction;

    //[SerializeField] private Building _building;
    //private BuildingTower _buildingTower;

    /*private void Awake()
    {
        //GetComponent<BuildingTower>
        Debug.Log("Awake Building");
        //_buildingTower = new BuildingTower(_building);
    }*/

    public int a;

    public void StartBuilding(GameObject obj)
    {
        if (_readyForConstruction)
        {
            Instantiate(obj, _platform.transform.position, _platform.transform.rotation);
            _platform.gameObject.SetActive(false);
            _readyForConstruction = false;
        }
        
    }

    private void OnMouseDown()
    {
        GetComponent<Renderer>().material.color = Color.white;

        

        _readyForConstruction = true;
    }
}
