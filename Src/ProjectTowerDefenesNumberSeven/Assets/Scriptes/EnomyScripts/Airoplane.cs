using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "BoxFromAiroplane", fileName = "SeafFromAiroplane")]
public class Airoplane : ScriptableObject
{
    public List<GameObject> _planes;
    //public List<Plane> _planes;
}
/*
[System.Serializable]

public class Plane
{
    [SerializeField] private int _number;
    [SerializeField] private GameObject _objPlane;

    public int Number => _number;

    public GameObject Obj => _objPlane;
}*/