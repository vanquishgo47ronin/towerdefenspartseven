using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class LeavePehota : MonoBehaviour
{
    [SerializeField] private GameObject _objPeh;
    [SerializeField] private Slider _livPehota;

    public void MinusLivPehota()
    {
        _livPehota.maxValue -= 10;
        if (_livPehota.maxValue == 0)
        {
            Destroy(_objPeh);
        }
    }
}
