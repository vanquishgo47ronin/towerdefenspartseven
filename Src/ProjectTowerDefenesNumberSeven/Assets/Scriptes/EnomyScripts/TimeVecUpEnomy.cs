using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeVecUpEnomy : MonoBehaviour
{
    [SerializeField] private bool _yep = true;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("Pousa1");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerable Pousa1()
    {
        while (_yep == true)
        {
            yield return new WaitForSeconds(10f);
            if (_yep == false)
            {
                StopCoroutine("Pousa1");
            }
        }
    }
}
