using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class LeaveTank : MonoBehaviour
{
    [SerializeField] private GameObject _objTank;
    [SerializeField] private Slider _livTank;

    public void MinusLivTank()
    {
        _livTank.maxValue -= 20;
        if (_livTank.maxValue == 0)
        {
            Destroy(_objTank);
        }
    }
}
