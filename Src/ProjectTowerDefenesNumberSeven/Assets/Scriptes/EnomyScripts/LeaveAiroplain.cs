using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class LeaveAiroplain : MonoBehaviour
{
    [SerializeField] private GameObject _obj;
    [SerializeField] private Slider _livAiroplain;

    public void MinusLivAiroplain()
    {
        _livAiroplain.maxValue -= 7;
        if (_livAiroplain.maxValue == 0)
        {
            Destroy(_obj);
        }
    }
}
