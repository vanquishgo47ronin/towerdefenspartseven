using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "BoxFromPehota", fileName = "SeafFromPehota")]
public class Pehota : ScriptableObject
{
    public List<GameObject> _pehota;
    //public List<SpaunPeh> _pehota;
}

/*
[System.Serializable]

public class SpaunPeh
{
    [SerializeField] private int _number;
    [SerializeField] private GameObject _objPehota;

    public int Number => _number;

    public GameObject Obj => _objPehota;
}
*/