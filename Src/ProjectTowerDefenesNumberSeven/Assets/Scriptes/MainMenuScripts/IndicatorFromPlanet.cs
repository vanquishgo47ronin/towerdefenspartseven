using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class IndicatorFromPlanet : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _text;

    [SerializeField] private SafeWithPlanets _safe;

    // Start is called before the first frame update
    void Start()
    {
        _text.text = $"{_safe.CountPlanets}";
    }

    // Update is called once per frame
    void Update()
    {
        _text.text = $"{_safe.CountPlanets}";
    }
}
