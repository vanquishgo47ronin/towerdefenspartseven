using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ActiveButton
{
    // ��������
    public delegate void OnClikePlayed();
    public delegate void OnClikeBacked();

    private Canvas _canvasMenu;
    private Canvas _canvasInprove;
    private Canvas _canvasLevels;

    private Button _play;
    private Button _exit;
    private Button _back;

    public ActiveButton(Canvas menu, Canvas inprove, Canvas levels, Button play, Button exit, Button back)
    {
        _canvasMenu = menu;
        _canvasInprove = inprove;
        _canvasLevels = levels;
        _play = play;
        _exit = exit;
        _back = back;
    }

    private void Start()
    {
        /*OnClikePlayed _onClikePlayed = null;
        _onClikePlayed += OnClikePlay;

        OnClikeBacked _onClikeBacked = null;
        _onClikeBacked += OnClikeBack;


        _onClikePlayed?.Invoke();
        _onClikeBacked?.Invoke();*/
    }

    public void OnClikePlay()
    {
        _canvasMenu.gameObject.SetActive(false);
        _canvasLevels.gameObject.SetActive(true);
    }

    public void OnClikeBackInMainMenu()
    {
        _canvasLevels.gameObject.SetActive(false);
        _canvasMenu.gameObject.SetActive(true);
    }

    public void OnCklickeImproveman()
    {
        _canvasLevels.gameObject.SetActive(false);
        _canvasInprove.gameObject.SetActive(true);
    }

    public void OnClikeBackInMenuLevel()
    {
        _canvasInprove.gameObject.SetActive(false);
        _canvasLevels.gameObject.SetActive(true);
    }
}
