using System.Collections.Generic;
using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;
using SceneList;
using System;


public class ButtonTransitition : MonoBehaviour, IPointerClickHandler, IPointerExitHandler
{
    [SerializeField] private Image _image;
    [SerializeField] private Scenes _scene;

    //private WaveIndicator _waveIndicator;

    private void Start()
    {
        //_waveIndicator = GetComponent<WaveIndicator>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        _image.transform.localScale = new Vector2(1.1f, 1.1f);
        ChacksForAmatch(_scene);
    }

    private void ChacksForAmatch(Scenes sce)
    {
        if (sce == Scenes.LEVEL1)
        {
            //_waveIndicator._index = 0;
            //_waveIndicator._waveLimit = 2;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        else if (sce == Scenes.LEVEL2)
        {
            //_waveIndicator._index = 0;
            //_waveIndicator._waveLimit = 4;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 3);
        }
        else if (sce == Scenes.LEVEL3)
        {
            //_waveIndicator._index = 0;
            //_waveIndicator._waveLimit = 6;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 4);
        }
        else if (sce == Scenes.LEVEL4)
        {
            //_waveIndicator._index = 0;
            //_waveIndicator._waveLimit = 8;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 5);
        }
        else if (sce == Scenes.LEVEL5)
        {
            //_waveIndicator._index = 0;
            //_waveIndicator._waveLimit = 10;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 6);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _image.transform.localScale = new Vector2(1f, 1f);
    }
}

