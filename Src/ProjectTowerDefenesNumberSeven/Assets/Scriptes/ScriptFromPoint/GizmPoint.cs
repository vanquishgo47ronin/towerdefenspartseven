using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GizmPoint : MonoBehaviour
{
    public int x = Random.Range(0,10);

    /*private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        
    }*/

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(transform.position, x);
    }
}
