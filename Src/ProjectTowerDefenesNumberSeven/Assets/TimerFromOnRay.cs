using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class TimerFromOnRay : MonoBehaviour
{
    [SerializeField] private Button _butonFromRay;

    public float _maxTime = 10000f;

    public float _minTime = 0;

    // Start is called before the first frame update
    void Start()
    {
        _minTime = 0;
    }

    // Update is called once per frame
    void Update()
    {
        _minTime += 1f;

        if (_minTime == 1000)
        {
            _butonFromRay.image.fillAmount += 0.1f;
        }
        else if (_minTime == 2000)
        {
            _butonFromRay.image.fillAmount += 0.1f;
        }
        else if (_minTime == 3000)
        {
            _butonFromRay.image.fillAmount += 0.1f;
        }
        else if (_minTime == 4000)
        {
            _butonFromRay.image.fillAmount += 0.1f;
        }
        else if (_minTime == 5000)
        {
            _butonFromRay.image.fillAmount += 0.1f;
        }
        else if (_minTime == 6000)
        {
            _butonFromRay.image.fillAmount += 0.1f;
        }
        else if (_minTime == 7000)
        {
            _butonFromRay.image.fillAmount += 0.1f;
        }
        else if (_minTime == 8000)
        {
            _butonFromRay.image.fillAmount += 0.1f;
        }
        else if (_minTime == 9000)
        {
            _butonFromRay.image.fillAmount += 0.1f;
        }
        else if (_minTime == 10000)
        {
            _butonFromRay.image.fillAmount += 0.1f;
            _minTime = 0;
        }

        if (_butonFromRay.image.fillAmount == 1f)
        {
            _butonFromRay.interactable = true;
            this.enabled = false;
        }
    }
}
