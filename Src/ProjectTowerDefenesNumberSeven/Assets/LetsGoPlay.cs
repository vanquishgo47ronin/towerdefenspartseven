using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SceneList;

public class LetsGoPlay : MonoBehaviour
{
    [SerializeField] private WaveIndicator _waveIndicator;

    [SerializeField] private GameObject _butonStartWaves;

    [SerializeField] private MainWaveBalanseInGame _mainWaveBalanseInGame;

    [SerializeField] private GeneralisemusWayweas _generalisemusWayweas;

    [SerializeField] private WaveWatcher _waveWatcher;

    [SerializeField] private Scenes _scena;

    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ComonGoPlaing()
    {
        _waveIndicator.Changes�heWaves();
        _mainWaveBalanseInGame.GoNewWave(1,_scena);
        //_generalisemusWayweas.NewWayev();
        _generalisemusWayweas.SpawnWave();
        _waveWatcher.enabled = true;
        _butonStartWaves.SetActive(false);
    }
}
