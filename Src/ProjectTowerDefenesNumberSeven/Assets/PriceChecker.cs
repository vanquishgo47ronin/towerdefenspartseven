using System.Collections;
using System.Collections.Generic;
using NamTowers;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class PriceChecker : MonoBehaviour
{
    [SerializeField] private CrystalBank _crystalBank;

    [SerializeField] private TextMeshProUGUI _textUpgrate;

    [SerializeField] private Button _buttonUpgrate;

    [SerializeField] private NameTowers _nameTowers;
    [SerializeField] private LevelTowers _level;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        CheckForConditions();
    }

    private void CheckForConditions()
    {
        if (_level == LevelTowers.LEVEL1 && _nameTowers == NameTowers.MachineTower && _crystalBank.Resources >= 140)
        {
            _textUpgrate.color = new Color32(255, 255, 255, 255);
            _buttonUpgrate.interactable = true;
        }
        else if (_level == LevelTowers.LEVEL1 && _nameTowers == NameTowers.MachineTower && _crystalBank.Resources < 140)
        {
            _textUpgrate.color = new Color32(255, 32, 0, 255);
            _buttonUpgrate.interactable = false;
        }

        if (_level == LevelTowers.LEVEL2 && _nameTowers == NameTowers.MachineTower && _crystalBank.Resources >= 180)
        {
            _textUpgrate.color = new Color32(255, 255, 255, 255);
            _buttonUpgrate.interactable = true;
        }
        else if (_level == LevelTowers.LEVEL2 && _nameTowers == NameTowers.MachineTower && _crystalBank.Resources < 180)
        {
            _textUpgrate.color = new Color32(255, 32, 0, 255);
            _buttonUpgrate.interactable = false;
        }

        if (_level == LevelTowers.LEVEL1 && _nameTowers == NameTowers.AntiTank && _crystalBank.Resources >= 300)
        {
            _textUpgrate.color = new Color32(255, 255, 255, 255);
            _buttonUpgrate.interactable = true;
        }
        else if (_level == LevelTowers.LEVEL1 && _nameTowers == NameTowers.AntiTank && _crystalBank.Resources < 300)
        {
            _textUpgrate.color = new Color32(255, 32, 0, 255);
            _buttonUpgrate.interactable = false;
        }

        if (_level == LevelTowers.LEVEL2 && _nameTowers == NameTowers.AntiTank && _crystalBank.Resources >= 350)
        {
            _textUpgrate.color = new Color32(255, 255, 255, 255);
            _buttonUpgrate.interactable = true;
        }
        else if (_level == LevelTowers.LEVEL2 && _nameTowers == NameTowers.AntiTank && _crystalBank.Resources < 350)
        {
            _textUpgrate.color = new Color32(255, 32, 0, 255);
            _buttonUpgrate.interactable = false;
        }

        if (_level == LevelTowers.LEVEL1 && _nameTowers == NameTowers.Plazmagan && _crystalBank.Resources >= 400)
        {
            _textUpgrate.color = new Color32(255, 255, 255, 255);
            _buttonUpgrate.interactable = true;
        }
        else if (_level == LevelTowers.LEVEL1 && _nameTowers == NameTowers.Plazmagan && _crystalBank.Resources < 400)
        {
            _textUpgrate.color = new Color32(255, 32, 0, 255);
            _buttonUpgrate.interactable = false;
        }
        
        if (_level == LevelTowers.LEVEL2 && _nameTowers == NameTowers.Plazmagan && _crystalBank.Resources >= 450)
        {
            _textUpgrate.color = new Color32(255, 255, 255, 255);
            _buttonUpgrate.interactable = true;
        }
        else if (_level == LevelTowers.LEVEL2 && _nameTowers == NameTowers.Plazmagan && _crystalBank.Resources < 450)
        {
            _textUpgrate.color = new Color32(255, 32, 0, 255);
            _buttonUpgrate.interactable = false;
        }
        
        if (_level == LevelTowers.LEVEL1 && _nameTowers == NameTowers.AntiFly && _crystalBank.Resources >= 190)
        {
            _textUpgrate.color = new Color32(255, 255, 255, 255);
            _buttonUpgrate.interactable = true;
        }
        else if (_level == LevelTowers.LEVEL1 && _nameTowers == NameTowers.AntiFly && _crystalBank.Resources < 190)
        {
            _textUpgrate.color = new Color32(255, 32, 0, 255);
            _buttonUpgrate.interactable = false;
        }

        if (_level == LevelTowers.LEVEL2 && _nameTowers == NameTowers.AntiFly && _crystalBank.Resources >= 230)
        {
            _textUpgrate.color = new Color32(255, 255, 255, 255);
            _buttonUpgrate.interactable = true;
        }
        else if (_level == LevelTowers.LEVEL2 && _nameTowers == NameTowers.AntiFly && _crystalBank.Resources < 230)
        {
            _textUpgrate.color = new Color32(255, 32, 0, 255);
            _buttonUpgrate.interactable = false;
        }
    }
}
