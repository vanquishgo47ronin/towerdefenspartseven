using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WaveIndicator : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _text;

    

    public int _index = 0;

    public int _waveLimit;

    private void Start()
    {
        _text.text = $"{_index} / {_waveLimit}";
    }

    public void Changes�heWaves()
    {
        _index++;
        if (_index < _waveLimit || _index == _waveLimit)
        {
            _text.text = $"{_index} / {_waveLimit}";
        }

        
    }
}
