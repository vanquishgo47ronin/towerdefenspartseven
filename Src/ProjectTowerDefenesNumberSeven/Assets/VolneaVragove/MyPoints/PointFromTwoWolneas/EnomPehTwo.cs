using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnomPehTwo : MonoBehaviour
{
    [SerializeField] private ParticleSystem _deathFromBfg;

    [SerializeField] private ParticleSystem _deathEfect;

    [SerializeField] private GameObject _obolochka;

    [SerializeField] private CrystalBank _crystalBank;

    [SerializeField] private GameObject _objPeh;

    [Header("Attrutes")]

    [SerializeField] private int _coines = 50;

    [SerializeField] private float _speed = 10f;

    [SerializeField] private float _distantya = 0.2f;

    [SerializeField] private Slider _livPehota;

    [SerializeField] private PointArrayTwo _array;
    private Transform _target;
    private int _waveintIndex = 0; // ������

    //private PointArray _myWay; //

    // Start is called before the first frame update
    void Start()
    {
        _target = PointArrayTwo._pointes[0];
        //_target = PointArray._pointes[0]; // ��� ������ �� �������� ������ �� �������
    }
    /*
    public void SetWay(PointArray valu) // ��� ������������ �����
    {
        _myWay = valu;
        _target = _myWay.pts[0];
    }*/

    // Update is called once per frame
    void Update()
    {
        if (_target == null) return;

        Vector3 _dir = _target.position - transform.position; // ����������� ������� ������ � �������. ����� �������� ������.

        // ����� �����������                                         //�����, ��� �� ��������� ������������ ����.
        transform.Translate(_dir.normalized * _speed * Time.deltaTime, Space.World);


        if (Vector3.Distance(transform.position, _target.position) <= _distantya)
        {
            _objPeh.transform.rotation = _target.rotation;
            GetNextWayPoint();
        }
    }

    private void GetNextWayPoint()
    {
        //if (_waveintIndex >= PointArray._pointes.Length - 1)
        //if (_waveintIndex >= _myWay.pts.Length - 1)
        if (_waveintIndex >= PointArrayTwo._pointes.Length - 1)
        {
            Destroy(this.gameObject);
            return;
        }

        _waveintIndex++;
        _target = PointArrayTwo._pointes[_waveintIndex]; // ����� ����� ������.
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("MachinBulineBlue"))
        {
            _livPehota.maxValue -= 5;
            if (_livPehota.maxValue == 0)
            {
                _livPehota.gameObject.SetActive(false);
                _speed = 0f;
                //_deathEfect.Play();
                //GameObject _efectDeatheaing = (GameObject)Instantiate(_deathEfect.gameObject, transform.position, transform.rotation);
                GameObject _efectDeatheaing = Instantiate(_deathEfect.gameObject, _objPeh.transform.position, _objPeh.transform.rotation);
                Debug.Log("� ����");

                //_deathEfect.Play();
                Destroy(_obolochka, 3f);
                Destroy(_objPeh, 3f);
                //Destroy(_objPeh, 3f);

                _crystalBank.Resources += _coines;
                _objPeh.SetActive(false);
            }
        }

        if (other.gameObject.CompareTag("FinalStation"))
        {
            //Destroy(this.gameObject);
            Destroy(_obolochka);
            this.gameObject.SetActive(false);
        }

        if (other.gameObject.CompareTag("AttakeMyRay"))
        {
            _crystalBank.Resources += _coines;
            //Destroy(this.gameObject);
            Destroy(_obolochka);

            this.gameObject.SetActive(false);
        }

        if (other.gameObject.CompareTag("AttakeMyRayBfg"))
        {
            _crystalBank.Resources += _coines;
            //Destroy(this.gameObject);

            Destroy(_obolochka);

            this.gameObject.SetActive(false);
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("AttakeMyRayBfg"))
        {
            _crystalBank.Resources += _coines;
            //Destroy(this.gameObject);

            Destroy(_obolochka);

            this.gameObject.SetActive(false);
        }

        if (collision.gameObject.CompareTag("AttakeMyRay"))
        {
            _livPehota.maxValue -= 1;

            if (_livPehota.maxValue == 0)
            {
                _crystalBank.Resources += 50;
                //Destroy(this.gameObject);
                Destroy(_obolochka);
                _objPeh.SetActive(false);
                //Destroy(_objPeh);
            }
        }
    }
}
