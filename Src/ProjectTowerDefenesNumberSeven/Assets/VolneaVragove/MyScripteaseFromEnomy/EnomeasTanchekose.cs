using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class EnomeasTanchekose : MonoBehaviour
{
    [SerializeField] private CrystalBank _crystalBank;

    [SerializeField] private GameObject _objPeh;

    [Header("Attrutes")]

    [SerializeField] private int _coines = 50;

    [SerializeField] private float _speed = 10f;

    [SerializeField] private float _distantya = 0.2f;

    [SerializeField] private Slider _livPehota;

    [SerializeField] private PointArray _array;

    private Transform _target;
    private int _waveintIndex = 0; // ������

    // Start is called before the first frame update
    void Start()
    {
        _target = PointArray._pointes[0]; // ��� ������ �� �������� ������ �� �������
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 _dir = _target.position - transform.position; // ����������� ������� ������ � �������. ����� �������� ������.

        // ����� �����������                                         //�����, ��� �� ��������� ������������ ����.
        transform.Translate(_dir.normalized * _speed * Time.deltaTime, Space.World);


        if (Vector3.Distance(transform.position, _target.position) <= _distantya)
        {
            _objPeh.transform.rotation = _target.rotation;
            GetNextWayPoint();
        }
    }

    private void GetNextWayPoint()
    {
        if (_waveintIndex >= PointArray._pointes.Length - 1)
        {
            Destroy(this.gameObject);
            return;
        }

        _waveintIndex++;
        _target = PointArray._pointes[_waveintIndex]; // ����� ����� ������.
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("MachinBulineBlue"))
        {
            _livPehota.maxValue -= 5;
            if (_livPehota.maxValue == 0)
            {
                Destroy(_objPeh);
                _crystalBank.Resources += _coines;
                _objPeh.SetActive(false);
            }
        }

        if (other.gameObject.CompareTag("FinalStation"))
        {
            //Destroy(this.gameObject);
            this.gameObject.SetActive(false);
        }

        if (other.gameObject.CompareTag("AttakeMyRay"))
        {
            _crystalBank.Resources += _coines;
            this.gameObject.SetActive(false);
        }
    }

    private void OnCollisionStay(Collision collision)
    {

        if (collision.gameObject.CompareTag("AttakeMyRay"))
        {
            _livPehota.maxValue -= 1;

            if (_livPehota.maxValue == 0)
            {
                _crystalBank.Resources += 50;
                Destroy(this.gameObject);
                _objPeh.SetActive(false);
                //Destroy(_objPeh);
            }
        }
    }
}
