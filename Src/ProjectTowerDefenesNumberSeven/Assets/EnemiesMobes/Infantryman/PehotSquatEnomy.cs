using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PathCreation;

public class PehotSquatEnomy : MonoBehaviour
{
    [SerializeField] private CrystalBank _crystalBank;
    [SerializeField] private PathCreator _pathCreator;

    [SerializeField] private GameObject _objPeh;
    [SerializeField] private Slider _livPehota;

    //[SerializeField] private GameObject _enomy;

    public float _speed = 0.1f;

    private float _destination;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _destination += _speed * Time.deltaTime;
        transform.position = _pathCreator.path.GetPointAtDistance(_destination);
        transform.rotation = _pathCreator.path.GetRotationAtDistance(_destination);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("MachinBulineBlue"))
        {
            _livPehota.maxValue -= 5;
            if (_livPehota.maxValue == 0)
            {
                //Destroy(_objPeh);
                _crystalBank.Resources += 50;
                _objPeh.SetActive(false);
            }
        }

        if (other.gameObject.CompareTag("FinalStation"))
        {
            //Destroy(this.gameObject);
            this.gameObject.SetActive(false);
        }

        if (other.gameObject.CompareTag("AttakeMyRay"))
        {
            _crystalBank.Resources += 50;
            this.gameObject.SetActive(false);
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        
        if (collision.gameObject.CompareTag("AttakeMyRay"))
        {
            _livPehota.maxValue -= 1;

            if (_livPehota.maxValue == 0)
            {
                _crystalBank.Resources += 50;
                _objPeh.SetActive(false);
                //Destroy(_objPeh);
            }
        }
    }
}
