using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ActiveOffButtunManegare : MonoBehaviour
{
    [SerializeField] private Button _buttonMachine;
    [SerializeField] private Button _buttonAntiTank;
    [SerializeField] private Button _buttonPlazmagan;
    [SerializeField] private Button _buttonAntiFly;

    private void Awake()
    {
        _buttonMachine.interactable = false;
        _buttonAntiTank.interactable = false;
        _buttonPlazmagan.interactable = false;
        _buttonAntiFly.interactable = false;
    }

    public void ActivateButtons(bool onActive)
    {
        if (onActive == true)
        {
            _buttonMachine.interactable = true;
            _buttonAntiTank.interactable = true;
            _buttonPlazmagan.interactable = true;
            _buttonAntiFly.interactable = true;
        }
        else if (onActive != true)
        {
            _buttonMachine.interactable = false;
            _buttonAntiTank.interactable = false;
            _buttonPlazmagan.interactable = false;
            _buttonAntiFly.interactable = false;
        }
    }
}
