using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class LiveRaket : MonoBehaviour
{
    [SerializeField] private TreeUpgrInGame _treeUpgrInGame;

    //[SerializeField] private GameObject _tree;

    //[SerializeField] private GameObject _playCanvas;

    [SerializeField] private Color _startColor;

    [SerializeField] private Color _haverColor;

    private Renderer _rend;

    public int _liv = 25;
    [SerializeField] private TextMeshProUGUI _text;
    [SerializeField] private GameObject _raketa;

    [SerializeField] private GameObject _menuDeath;

    [SerializeField] private ResultManager _resultManager;

    [SerializeField] private ParticleSystem _aiKaboomBoomOne;
    [SerializeField] private ParticleSystem _aiKaboomBoomTwo;
    [SerializeField] private ParticleSystem _aiKaboomBoomThree;
    [SerializeField] private ParticleSystem _aiKaboomBoomFore;

    private void Start()
    {
        _aiKaboomBoomOne.Stop();
        _aiKaboomBoomTwo.Stop();
        _aiKaboomBoomThree.Stop();
        _aiKaboomBoomFore.Stop();
        _rend = GetComponent<Renderer>();
        _startColor = _rend.material.color;
    }

    private void OnMouseEnter()
    {
        _rend.material.color = _haverColor;
    }

    private void OnMouseDown()
    {
        _treeUpgrInGame.enabled = true;
        //_tree.SetActive(true);
    }

    private void OnMouseExit()
    {
        _rend.material.color = _startColor;
    }

    // Update is called once per frame
    void Update()
    {
        _text.text = $"{_liv}";
        if (_liv == 0)
        {
            _menuDeath.SetActive(true);
        }
    }

    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("EarthPeh") || other.CompareTag("EarthTank") || other.CompareTag("SkyAirplane") || other.CompareTag("Enomy"))
        {
            _liv -= 1;
            //_aiKaboomBoomOne.gameObject.SetActive(true);
            _aiKaboomBoomOne.Play();
            //_aiKaboomBoomOne.Stop();
            //_aiKaboomBoomTwo.gameObject.SetActive(true);
            _aiKaboomBoomTwo.Play();
            //_aiKaboomBoomTwo.Stop();
            //_aiKaboomBoomThree.gameObject.SetActive(true);
            _aiKaboomBoomThree.Play();
            //_aiKaboomBoomThree.Stop();
            //_aiKaboomBoomFore.gameObject.SetActive(true);
            _aiKaboomBoomFore.Play();
            //_aiKaboomBoomFore.Stop();
            /*
            _aiKaboomBoomOne.gameObject.SetActive(false);
            _aiKaboomBoomTwo.gameObject.SetActive(false);
            _aiKaboomBoomThree.gameObject.SetActive(false);
            _aiKaboomBoomFore.gameObject.SetActive(false);
            */
        }
    }

    public void OtpravitRezult()
    {
        _resultManager.MyRezulte();
    }

    /*private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("EarthPeh") || other.CompareTag("EarthTank") || other.CompareTag("SkyAirplane"))
        {
            _liv -= 1;
        }
    }*/
}
