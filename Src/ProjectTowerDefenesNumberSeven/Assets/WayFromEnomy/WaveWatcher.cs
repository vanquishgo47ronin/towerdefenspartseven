using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SceneList;

public class WaveWatcher : MonoBehaviour
{
    [SerializeField] private ResultManager _resultManager;

    [SerializeField] private MainWaveBalanseInGame _mainWaveBalanseInGame;

    [SerializeField] private WaveIndicator _waveIndicator;

    [SerializeField] private GeneralisemusWayweas _GeneralisemusWayweas;

    [SerializeField] private GameObject _oblasteSpawna;

    [SerializeField] private GameObject _canvasVictory;

    public float _minTime = 0;

    public float _maxTime = 100;

    //[SerializeField] private LiveRaket _liveRaket;

    public bool _activeGo = false;

    private float _score = 0;

    private int x = 1;

    [SerializeField] private Scenes _scena;

    /*

    // ������ ��� ��������� ������ ����� 1.
    [SerializeField] private Transform _arsenalmyPehEvent;

    // ���� ��� ��������� � �� ���� ��������� �����.
    [SerializeField] private EnomyVersioTwoZero _enomyVersioTwoZero;

    [SerializeField] private List<GameObject> _obj;

    */

    // Start is called before the first frame update
    void Start()
    {
        //_resultManager.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (_oblasteSpawna.transform.childCount == 0)
        {
            _activeGo = true;
        }

        if (_activeGo == true)
        {
            _minTime += 1;
        }

        if (_minTime == _maxTime)
        {
            if (_waveIndicator._index == _waveIndicator._waveLimit && _oblasteSpawna.transform.childCount == 0)
            {
                Debug.Log("����������� ���������");
                Itog();
            }

            _minTime = 0;

            _activeGo = false;

            x++;

            _waveIndicator.Changes�heWaves();
            _mainWaveBalanseInGame.GoNewWave(x, _scena);

            //this.enabled = false;

            if (_activeGo == false)
            {
                //Itog();
            }

            //StartCoroutine("TheFinalItog");
        }

        
    }

    /*
    IEnumerable TheFinalItog()
    {
        Debug.Log("� ���");
        yield return new WaitForSeconds(0.5F);
        if (_waveIndicator._index == _waveIndicator._waveLimit && _oblasteSpawna.transform.childCount == 0)
        {
            Itog();
        }
    }*/

    public void Itog()
    {
        _resultManager.MyRezulte();
        _canvasVictory.SetActive(true);
    }
       // _liveRaket.OtpravitRezult();
    
}
