using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawWaveaEnom : MonoBehaviour
{
    public Transform _enomy;

    public float _timeBetweenWaves = 5f;

    private float countDown = 2f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (countDown <= 0f)
        {
            SpawnWave();
        }

        countDown -= Time.deltaTime;
    }

    private void SpawnWave()
    {

    }
}
