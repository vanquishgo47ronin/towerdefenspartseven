using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RaketaTrioTeo : MonoBehaviour
{
    public int _liv = 3;
    [SerializeField] private TextMeshProUGUI _text;

    [SerializeField] private GameObject _menuDeath;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _text.text = $"{_liv}";
        if (_liv == 0)
        {
            _menuDeath.SetActive(true);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("��!");
        _liv -= 1;
        Debug.Log(collision);
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("��!");
        Debug.Log(other);
    }
}
