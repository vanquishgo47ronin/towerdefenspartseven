using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class ChitatelHistory : MonoBehaviour
{
    /*
    [SerializeField] private Button _buttonOne;
    [SerializeField] private Button _buttonTwo;
    [SerializeField] private Button _buttonThree;
    [SerializeField] private Button _buttonFore;
    [SerializeField] private Button _buttonFive;
    [SerializeField] private Button _buttonSix;
    [SerializeField] private Button _buttonSeven;
    [SerializeField] private Button _buttonEaght;
    [SerializeField] private Button _buttonNine;
    [SerializeField] private Button _buttonTen;
    [SerializeField] private Button _buttonEleven;
    [SerializeField] private Button _buttonTwelve;
    [SerializeField] private Button _buttonThirteen;
    [SerializeField] private Button _buttonFourteen;
    [SerializeField] private Button _buttonFifteen;
    [SerializeField] private Button _buttonSixteen;*/
    [SerializeField] private GameObject _imageOne;
    [SerializeField] private GameObject _imageTwo;
    [SerializeField] private GameObject _imageThree;
    [SerializeField] private GameObject _imageFore;
    [SerializeField] private GameObject _imageFive;
    [SerializeField] private GameObject _imageSix;
    [SerializeField] private GameObject _imageSeven;
    [SerializeField] private GameObject _imageEaghto;
    [SerializeField] private GameObject _imageNine;
    [SerializeField] private GameObject _imageTen;
    [SerializeField] private GameObject _imageEleven;
    [SerializeField] private GameObject _imageTwelve;
    [SerializeField] private GameObject _imageThirteen;
    [SerializeField] private GameObject _imageFourteen;
    [SerializeField] private GameObject _imageFifteen;
    [SerializeField] private GameObject _imageSixteen;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OneList()
    {
        _imageTwo.SetActive(true);
        _imageOne.SetActive(false);
    }

    public void TwoList()
    {
        _imageThree.SetActive(true);
        _imageTwo.SetActive(false);
    }

    public void ThreeList()
    {
        _imageFore.SetActive(true);
        _imageThree.SetActive(false);
    }

    public void ForeList()
    {
        _imageFive.SetActive(true);
        _imageFore.SetActive(false);
    }

    public void FiveList()
    {
        _imageSix.SetActive(true);
        _imageFive.SetActive(false);
    }

    public void SixList()
    {
        _imageSeven.SetActive(true);
        _imageSix.SetActive(false);
    }

    public void SevenList()
    {
        _imageEaghto.SetActive(true);
        _imageSeven.SetActive(false);
    }

    public void EaghtList()
    {
        _imageNine.SetActive(true);
        _imageEaghto.SetActive(false);
    }

    public void NineList()
    {
        _imageTen.SetActive(true);
        _imageNine.SetActive(false);
    }

    public void TenList()
    {
        _imageEleven.SetActive(true);
        _imageTen.SetActive(false);
    }

    public void ElevenList()
    {
        _imageTwelve.SetActive(true);
        _imageEleven.SetActive(false);
    }

    public void TvelveList()
    {
        _imageThirteen.SetActive(true);
        _imageTwelve.SetActive(false);
    }

    public void TheartenList()
    {
        _imageFourteen.SetActive(true);
        _imageThirteen.SetActive(false);
    }

    public void FortinList()
    {
        _imageFifteen.SetActive(true);
        _imageFourteen.SetActive(false);
    }

    public void FiftenList()
    {
        _imageSixteen.SetActive(true);
        _imageFifteen.SetActive(false);
    }

    public void SixtineList()
    {
        _imageOne.SetActive(true);
        _imageSixteen.SetActive(false);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
