using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deselect : MonoBehaviour
{
    [SerializeField] private Canvas _canvasFromTower;

    private void OnMouseDown()
    {
        _canvasFromTower.gameObject.SetActive(false);
    }
}
