using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TuretTrioTeoGun : MonoBehaviour
{
    [SerializeField] private GameObject _stvolGunTrio;

    private Transform _target;

    [Header("Attributes")] // �������� ��� ���������.

    public float _range = 3f;
    // ����������������
    public float _fireRate = 1f;
    // �������� ����� ����
    public float _fireCountdown = 0f;

    public string _enemyTag = "Enomy";

    /*
    public string _enemyTagOne = "EarthPeh";
    public string _enemyTagTwo = "EarthTank";
    */

    public Transform _partToRotate;

    public float _turnSpeed = 10f;

    //������ ����
    public GameObject _bullite;

    //[SerializeField] private SpawnObjGunTrioBulite _spawn;

    // ������, ��� ����� �������� ����
    public Transform _oblastSpawn;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("UpdateTarget", 0F, 0.5f);
    }

    void UpdateTarget() // ���� ����� ����
    {
        GameObject[] _enemys = GameObject.FindGameObjectsWithTag(_enemyTag);
        float _shortDistance = Mathf.Infinity;
        GameObject _nearEnemy = null; // ��������� ����

        foreach (GameObject enem in _enemys)
        {
            float _distansy = Vector3.Distance(transform.position, enem.transform.position);

            if (_distansy < _shortDistance)
            {
                _shortDistance = _distansy;
                _nearEnemy = enem;
            }

            if (_distansy == 0)
            {
                
            }
        }

        if (_nearEnemy != null && _shortDistance <= _range)
        {
            _stvolGunTrio.transform.Rotate(new Vector3(0, 0, 0) * Time.deltaTime);
            _target = _nearEnemy.transform;
        }
        else
        {
            _target = null;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (_target == null)
        {
            
            return;
        }

        Vector3 _dir = _target.position - transform.position;
        Quaternion lookRotatetion = Quaternion.LookRotation(_dir);
        //Vector3 _rotation =  lookRotatetion.eulerAngles;
        Vector3 _rotation = Quaternion.Lerp(_partToRotate.rotation, lookRotatetion, Time.deltaTime * _turnSpeed).eulerAngles;

        _partToRotate.rotation = Quaternion.Euler(_rotation.x, _rotation.y, _rotation.z);


        // ��������
        if (_fireCountdown <= 0f)
        {
            Shoot();
            _fireCountdown = 1f / _fireRate; // ����� ������ ������� ��������.
        }

        // �������� ������
        _fireCountdown -= Time.deltaTime;
    }

    private void Shoot()
    {
        Debug.Log("Shoot!");
        //_spawn.GetBulieatMachine();
        Instantiate(_bullite, _oblastSpawn.position, _oblastSpawn.rotation);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, _range);
    }
}
