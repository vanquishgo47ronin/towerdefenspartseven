using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class TimerFromOnButtonGunTrioTeo : MonoBehaviour
{
    [SerializeField] private Button _butonTrio;

    public float _maxTime = 3000;

    public float _minTime = 0;

    /*private void Awake()
    {
        StartCoroutine("ActiveadButonHelpGun");
    }*/

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Update()
    {
        _minTime += 1;

        if (_maxTime == 500)
        {
            _butonTrio.image.fillAmount += 0.2f;
        }
        else if (_maxTime == 1000)
        {
            _butonTrio.image.fillAmount += 0.2f;
        }
        else if (_maxTime == 1500)
        {
            _butonTrio.image.fillAmount += 0.2f;
        }
        else if (_maxTime == 2000)
        {
            _butonTrio.image.fillAmount += 0.2f;
        }
        else if (_maxTime == 2500)
        {
            _butonTrio.image.fillAmount += 0.2f;
        }

        if (_butonTrio.image.fillAmount == 1)
        {
            _minTime = 0;
            this.enabled = false;
        }
    }

    /*public void OnTimerPuse()
    {
        StartCoroutine("ActiveadButonHelpGun");
    }

    private IEnumerable ActiveadButonHelpGun()
    {
        _butonTrio.image.fillAmount = 0;

        _butonTrio.interactable = false;

        while (true)
        {
            _butonTrio.image.fillAmount += 0.1f;

            if (_butonTrio.image.fillAmount == 1)
            {
                _butonTrio.interactable = true;
                StartCoroutine("ActiveadButonHelpGun");
            }
            yield return new WaitForSeconds(0.1f);
        }
    }*/
}
