using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeadeBuliteGunTrioTeo : MonoBehaviour
{
    public float _speed = 0;

    public float _myTimer = 0f;
    private float _maxSecond = 2000f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Impules();

        if (_myTimer == _maxSecond)
        {
            Destroy(this.gameObject);
            //this.gameObject.SetActive(false);
            _myTimer = 0;
        }
        _myTimer += 1;
    }

    public void Impules()
    {
        transform.Translate(Vector3.forward * _speed * Time.deltaTime);
    }
}
