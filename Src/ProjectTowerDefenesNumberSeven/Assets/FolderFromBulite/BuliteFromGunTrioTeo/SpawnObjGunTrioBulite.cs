using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObjGunTrioBulite : MonoBehaviour
{
    // ������ ��� ��������� ���� �� �����.
    [SerializeField] private Transform _arsenalBulite;

    // ���� ��� ��������� � �� ���� ��������� ����.
    [SerializeField] private BullitetFromGunTrioTeo _bullitetFromGunTrioTeo;

    [SerializeField] private List<GameObject> _obj;

    private void Awake()
    {
        Initialization();
    }

    private void Initialization()
    {
        foreach (GunTrioTeoBulit obj in _bullitetFromGunTrioTeo._GunTrioTeoBulit)
        {
            for(int i = 0; i < obj.Number; i++)
            {
                GameObject _gameObj = Instantiate(obj.Obj, _arsenalBulite); // ������� � ��������.

                _obj.Add(_gameObj); // ��������� � ������ ����.

                _gameObj.SetActive(false); // ��������� ��.
            }
        }
    }

    // ����� ��� ��������� ��������.
    public GameObject GetBulieat()
    {
        
        foreach (GameObject _gaeobjetMyTrio in _obj) // ���������� ������� � �����
        {
            if (!_gaeobjetMyTrio.gameObject.activeInHierarchy) // �������� ������� ������
            {
                _gaeobjetMyTrio.transform.position = _arsenalBulite.position;

                _gaeobjetMyTrio.SetActive(true);
                return _gaeobjetMyTrio;
            }
        }
        
        return null;
    }
}
