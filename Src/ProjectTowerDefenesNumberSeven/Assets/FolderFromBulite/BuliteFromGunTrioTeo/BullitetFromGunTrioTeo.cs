using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "BoxGunTrioTeoBullite", fileName = "ArsenalBuliteGunTrioTeo")]
public class BullitetFromGunTrioTeo : ScriptableObject
{
    public List<GunTrioTeoBulit> _GunTrioTeoBulit;
}

[System.Serializable]
public class GunTrioTeoBulit
{
    [SerializeField] private int _numberGunTrioTeo;
    [SerializeField] private GameObject _objBuliteGunTrioTeo;

    public int Number => _numberGunTrioTeo;

    public GameObject Obj => _objBuliteGunTrioTeo;
}