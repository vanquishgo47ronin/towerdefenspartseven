using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "BoxMachineBullite", fileName = "ArsenalBuliteMachine")]
public class BulliteFromMachineGun : ScriptableObject
{
    public List<MachBuleit> _machineBulite;
}

[System.Serializable]
public class MachBuleit
{
    [SerializeField] private int _numberMach;
    [SerializeField] private GameObject _objBuliteMach;

    public int Number => _numberMach;

    public GameObject Obj => _objBuliteMach;
}
