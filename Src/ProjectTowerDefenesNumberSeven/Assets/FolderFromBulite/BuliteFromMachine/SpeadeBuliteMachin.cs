using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeadeBuliteMachin : MonoBehaviour
{
    [SerializeField] private GameObject _objBluePluh;

    public float _speed = 2;

    public float _myTimer = 0f;
    private float _maxSecond = 1300f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Impules();

        if (_myTimer == _maxSecond)
        {
            Destroy(this.gameObject);
            //this.gameObject.SetActive(false);
            _myTimer = 0;
        }
        _myTimer += 1;
    }

    public void Impules()
    {
        transform.Translate(Vector3.forward * _speed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enomy") || other.gameObject.CompareTag("Enomy"))
        {
            GameObject _efectDeatheaing = Instantiate(_objBluePluh.gameObject, this.gameObject.transform.position, this.gameObject.transform.rotation);
            Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Destroy(this.gameObject);
    }
}
