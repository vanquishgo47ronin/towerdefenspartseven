using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "BoxPlazmeBullite", fileName = "ArsenalBulitePlazme")]
public class BulliteFromPlazmagane : ScriptableObject
{
    public List<PlazmaganeBulit> _plazmaganeBulite;
}

[System.Serializable]
public class PlazmaganeBulit
{
    [SerializeField] private int _numberPlazme;
    [SerializeField] private GameObject _objBulitePlazme;

    public int Number => _numberPlazme;

    public GameObject Obj => _objBulitePlazme;
}