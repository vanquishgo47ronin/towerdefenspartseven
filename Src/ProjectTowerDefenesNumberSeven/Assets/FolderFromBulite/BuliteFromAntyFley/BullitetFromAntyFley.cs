using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "BoxAntyFleyBullite", fileName = "ArsenalBuliteAntyFley")]
public class BullitetFromAntyFley : ScriptableObject
{
    public List<AntyFleyBulit> _antyFleyeBulite;
}

[System.Serializable]
public class AntyFleyBulit
{
    [SerializeField] private int _numberAntyFley;
    [SerializeField] private GameObject _objBuliteAntyFley;

    public int Number => _numberAntyFley;

    public GameObject Obj => _objBuliteAntyFley;
}