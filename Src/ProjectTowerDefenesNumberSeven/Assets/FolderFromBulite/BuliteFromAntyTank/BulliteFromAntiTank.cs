using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "BoxAntiTankBullite", fileName = "ArsenalBuliteAntiTank")]
public class BulliteFromAntiTank : ScriptableObject
{
    public List<AntiTankBulit> _antiTankBulite;
}

[System.Serializable]
public class AntiTankBulit
{
    [SerializeField] private int _numberAntiTank;
    [SerializeField] private GameObject _objBuliteAntiTank;

    public int Number => _numberAntiTank;

    public GameObject Obj => _objBuliteAntiTank;
}