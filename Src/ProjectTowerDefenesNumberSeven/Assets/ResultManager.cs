using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ResultManager : MonoBehaviour
{
    [SerializeField] private SafeWithPlanets _safeWithPlanets;

    [SerializeField] private Image _imageOne;
    [SerializeField] private Image _imageTwo;
    [SerializeField] private Image _imageThree;

    [SerializeField] private LiveRaket _live;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void MyRezulte()
    {
        if (_live._liv == 25 || _live._liv == 24 || _live._liv >= 23 || _live._liv >= 22 || _live._liv == 21 || _live._liv == 20 || _live._liv == 19 || _live._liv == 18 || _live._liv == 17 || _live._liv == 16 || _live._liv == 15)
        {
            _imageOne.gameObject.SetActive(true);
            _imageTwo.gameObject.SetActive(true);
            _imageThree.gameObject.SetActive(true);
            _safeWithPlanets.CountPlanets += 3;
        }
        else if (_live._liv == 14 || _live._liv == 13 || _live._liv == 12 || _live._liv == 11 || _live._liv == 10 || _live._liv == 9)
        {
            _imageOne.gameObject.SetActive(true);
            _imageTwo.gameObject.SetActive(true);
            _imageThree.gameObject.SetActive(false);
            _safeWithPlanets.CountPlanets += 2;
        }
        else if (_live._liv == 8 || _live._liv == 7 || _live._liv == 6 || _live._liv == 5 || _live._liv == 4 || _live._liv == 3 || _live._liv == 2 || _live._liv == 1)
        {
            _imageOne.gameObject.SetActive(true);
            _imageTwo.gameObject.SetActive(false);
            _imageThree.gameObject.SetActive(false);
            _safeWithPlanets.CountPlanets += 1;
        }
        /*
        //_canvasVictory.SetActive(true);
        if (x == 25 || x == 24 || x >= 23 || x >= 22 || x == 21 || x == 20 || x == 19 || x == 18 || x == 17 || x == 16 || x == 15)
        {
            _imageOne.gameObject.SetActive(true);
            _imageTwo.gameObject.SetActive(true);
            _imageThree.gameObject.SetActive(true);
            _safeWithPlanets.CountPlanets += 3;
        }
        else if (x == 14 || x == 13 || x == 12 || x == 11 || x == 10 || x == 9)
        {
            _imageOne.gameObject.SetActive(true);
            _imageTwo.gameObject.SetActive(true);
            _imageThree.gameObject.SetActive(false);
            _safeWithPlanets.CountPlanets += 2;
        }
        else if (x == 8 || x == 7 || x == 6 || x == 5 || x == 4 || x == 3 || x == 2 || x == 1)
        {
            _imageOne.gameObject.SetActive(true);
            _imageTwo.gameObject.SetActive(false);
            _imageThree.gameObject.SetActive(false);
            _safeWithPlanets.CountPlanets += 1;
        }*/
    }
}
